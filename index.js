//Bai 1: Tinh luong nhan vien
 //input: luong1ngay, soNgaylam
 //output: tongLuong
 //propress: tongLuong = luong1ngay * soNgaylam

 //input
 var luong1ngay = 100000;
 var soNgaylam = 28;

//propress
var tongLuong = luong1ngay * soNgaylam;

//output
console.log('tongLuong = ', tongLuong);

//-----------------------------------------------------------------------------------

//Bai 2: Tinh gia tri trung binh 
//input: (5 so thuc) number1, number2, number3, number4, number5
//output: trungBinh
//propress: trungBinh = (number1 + number2 + number3 + number4 + number5) / 5

//input
var number1 = 1;
var number2 = 2;
var number3 = 3;
var number4 = 4;
var number5 = 5;

//propress
var giaTritrungBinh = (number1 + number2 + number3 + number4 + number5) / 5;

//output
console.log('giaTritrungBinh = ', giaTritrungBinh);

//---------------------------------------------------------------------------------------

//Bai 3: Quy doi tien
//input: 1 usd =23.500vnd, USD nguoi dung nhap
//output: quyDoiVND
//propress: quyDoiVND = USD nguoi dung nhap * 23.500

//input
var quyDoiVNDdonvi1USD = 23500;
var USDnhapVao = 5;

// propress
var quyDoiVND = USDnhapVao * quyDoiVNDdonvi1USD;

// output
console.log('Người dùng nhập 5 USD = ', quyDoiVND ,'VND');

//-----------------------------------------------------------------------------------------

//Bai 4: Tinh dien tich, chu vi hinh chu nhat

//input: chieu dai, chieu rong 
//output: chu vi, dien tich
//propress: chu vi = (chieu dai + chieu rong)*2
//propress: dien tich = chieu dai * chioeu rong 

//input
var chieuRong = 5;
var chieuDai = 10;

// propress
var chuVi = (chieuRong + chieuDai) * 2;
var dienTich = chieuRong * chieuDai;

//output
console.log('Chu vi = ', chuVi );
console.log('Dien tich = ', dienTich);

//--------------------------------------------------------------------------------------------

//Bai 5: Tinh tong 2 ky so

//input: số number
//output: Tổng 2 ký số
//propress: số hàng đơn vị + số hàng chục

//input
var number = 58;
var soHangchuc = Math.floor(number / 10);
var soHangdonvi = Math.floor(number % 10);

//propress 
var tongKyso = soHangchuc + soHangdonvi;

// output
console.log('Tổng ký số = ', tongKyso );